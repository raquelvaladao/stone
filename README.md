
# Entregável 1
## Info sobre a API
- Feito em Java 11 com Spring 2.7.0 e Maven 3.8
- Documentação com OpenAPI
- Testes unitários e de integração
- PostgreSQL como banco
- Documentação da API no endpoint **/docs.html**


## Testes unitários, de integração e quality gate
- Foi utilizado quality gate default do SonarQube.
- API com 93% de line coverage, 90% de condition coverage, 0 Issues (sem majors, criticals, blockers, security hotspots)
- Os testes serão executados pela pipeline no stage 'test'
- Testes unitários parametrizados com JUnit 5 + Mockito/BDDMockito
- Testes de integração com MockMvc para o controller


## Containerização da aplicação e Docker compose
### Dockerfile
- Verificando possibilidades de otimização do Dockerfile..(TO-DO)


### Docker-compose
- API e PostgreSQL


## Pipeline
- Essa é uma explicação de alguns fatores do arquivo [.gitlab-ci.yml](.gitlab-ci.yml)
### Stage "test"
- É utilizado o Sonar para submeter os testes unitários e de integração ao quality gate. Temos 2 variáveis setadas que serão usadas na pipeline.
Uma é pro servidor do sonar, outro um token gerado no servidor para analisar o projeto em questão.
```bash
export SONAR_HOST_URL=http://${DOMAIN}:{cluster_port}
export SONAR_TOKEN=sqp_xxxxxx
```
